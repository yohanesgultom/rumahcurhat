'use strict';
module.exports = (sequelize, DataTypes) => {
  const Config = sequelize.define('Config', {
    key: {
      type: DataTypes.STRING,
      primaryKey: true,
      allowNull: false,      
    },
    value: DataTypes.TEXT
  }, {});
  Config.associate = function(models) {
    // associations can be defined here
  };
  return Config;
};