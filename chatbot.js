/**
 * Chat bot engine
 */

'use strict'

const models = require('./models/index')

class ChatBot {

    constructor(messages, followUpMessages, restartCommand) {
        this.messages = messages
        this.followUpMessages = followUpMessages
        this.restartCommand = restartCommand || 'ulangi bot'
    }

    getMessage(seq) {
        let msg = this.messages[seq]
        return msg ? { ...msg } : msg
    }

    getFollowUpMessage(seq) {
        let msg = this.followUpMessages[seq]
        return msg ? { ...msg } : msg
    }

    async chat(userId, msg, channel, seq) {
        if (!msg) {
            return undefined
        }

        channel = channel || 'FB'
        seq = seq || 0
        let next = null
        let initalData = {userId: userId, channel: channel, seq: seq}
        let result = await models.Session.findOrCreate({
            where: {userId: userId, channel: channel},
            order: [['id', 'DESC']],
            defaults: initalData
        })
        let session = result[0]

        // handle restart command
        if (msg.trim().toUpperCase() === this.restartCommand.trim().toUpperCase()) {
            session.seq = 1
            // if it is part of follow up, also update the followUpStatus
            if (session.followUpStatus === models.Session.FollowUpStatusCode.IN_PROGRESS) {
                session.followUpStatus = models.Session.FollowUpStatusCode.DONE    
            }
            await session.save()
            next = this.getMessage(session.seq)
            return next
        } else if (session.followUpStatus === models.Session.FollowUpStatusCode.IN_PROGRESS) {            
            // handle follow up reply
            let nextSeq = this.followUpMessages.QUESTION.options[msg]
            if (!nextSeq) {
                // unknown option, repeat following up question
                next = this.followUpMessages.QUESTION
            } else {
                if (nextSeq.trim().toUpperCase() === 'CONTINUE') {
                    // repeat last chat
                    next = this.getMessage(session.seq)
                } else {
                    // send good bye, reset sequence
                    next = this.getFollowUpMessage(nextSeq)
                    session.seq = 0
                }                
                session.followUpStatus = models.Session.FollowUpStatusCode.DONE
                await session.save()                
            }
            return next
        } else if (session.status !== models.Session.StatusCode.NEW) {
            // do not reply if flagged
            return undefined
        } else {
            // store all user inputs (message)    
            let m = {sessionId: session.id, text: msg}
            let lastChat = this.getMessage(session.seq) || null
            if (lastChat) {
                m.seq = session.seq
                m.replyTo = JSON.stringify(lastChat.messages)
                m.chat = JSON.stringify(lastChat)
            }
            await models.Message.create(m)

            if (!lastChat) {
                // if no last chat then return initial chat
                session.seq = 1        
                next = this.getMessage(session.seq)
            } else {
                if (lastChat.type === 'OPTIONS') {
                    // if message is not recognized
                    // repeat current chat
                    let nextSeq = this.getMessage(session.seq).options[msg]
                    session.seq = nextSeq === undefined ? session.seq : nextSeq
                } else if (lastChat.type === 'FREE') {
                    // capture as string
                    // because forcing to parse may limit user expression
                    // for example, these answers for age questions are valid:
                    // "17", "saya 17 tahun", "17 tahun"
                    session[lastChat.variable] = msg
                    session.seq = lastChat.next_seq                
                } else { // TRANSITION
                    // if not recognized, restart chat
                    session.seq = lastChat.next_seq || 0
                }
                // if next is transition,
                // accumulates message and appends them 
                // on next non-transition
                next = this.getMessage(session.seq)
                if (next) {
                    if (next.type === 'TRANSITION') {
                        let transitionMessages = []
                        while (next.type === 'TRANSITION') {
                            transitionMessages = transitionMessages.concat(next.messages)
                            session.seq = next.next_seq
                            if (this.messages[next.next_seq]) {
                                next = this.getMessage(next.next_seq)
                            } else {
                                break
                            }
                        }
                        next.messages = transitionMessages.concat(next.messages)
                    } else if (next.type === 'HANDOVER') {
                        // reset sequence 
                        // and set flag to stop replying
                        session.seq = 0
                        session.status = models.Session.StatusCode.HANDOVERED
                    }
                }

            }

            // update session
            await session.save()
            return next

        }

    }

}

module.exports = ChatBot