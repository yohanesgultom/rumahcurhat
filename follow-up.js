/**
 * Cron job to follow up neglected chat session. 
 * This job CAN be run as often as possible.
 * This job is NOT designed to be run on multiple parallel instances as it does not lock database.
 */

'use strict'

const models = require('./models/index'),
    Line = require('@line/bot-sdk'),
    FrontApp = require('./frontapp'),
    FacebookHelper = require('./facebook'),
    LineHelper = require('./line'), 
    Op = models.Sequelize.Op

class FollowUpJob {
    constructor(facebookHelper, lineHelper, chatFollowUp, minHour, now) {
        this.facebookHelper = facebookHelper
        this.lineHelper = lineHelper
        this.chatFollowUp = chatFollowUp
        this.minHour = minHour || 24
        this.now = now || new Date()        
    }

    getMinUpdatedAt() {
        return new Date(this.now.getTime() - this.minHour * 3600000)
    }

    async run() {      
        // get sessions that need follow up
        let sessions = await models.Session.findAll({limit: 100, order: ['id'], where: {
            followUpStatus: models.Session.FollowUpStatusCode.NEW, // has not been followed up
            status: models.Session.StatusCode.NEW, // not yet handovered
            seq: {[Op.gte]: 0}, // in the middle of chat with bot
            updatedAt: {[Op.lte]: this.getMinUpdatedAt()}, // updated at least minHour ago
        }})
        if (!this.chatFollowUp.QUESTION) {
            throw new Error('Please define chat with key QUESTION in CHAT_FOLLOW_UP config')
        }

        console.info(`${sessions.length} session(s) needs follow up`)
        let followUpChat = this.chatFollowUp.QUESTION
        for (let i = 0; i < sessions.length; i++) {
            let s = sessions[i]
            if (s.channel) {
                try {
                    // send follow up message and update session
                    let channelCode = s.channel.toUpperCase()                    
                    if (channelCode === 'FB') {
                        await this.facebookHelper.sendChat(s.userId, followUpChat)
                    } else if (channelCode === 'LINE') {
                        await this.lineHelper.sendChat(s.userId, followUpChat)
                    }
                    console.info(`Follow up message sent to ${s.userId}`)
                    s.followUpStatus = models.Session.FollowUpStatusCode.IN_PROGRESS
                    await s.save()                   
                } catch (e) {
                    console.error(e)
                }
            }
        }
        
    }
}

if (require.main !== module) {
    module.exports = FollowUpJob
} else {
    models.Config.findAll().then((configs) => {

        const config = {}
        configs.forEach(c => config[c.key] = c.value)
        
        // helpers
        let facebookHelper = {}
        if (config.FB_MESSAGING_API_URL && config.FB_APP_SECRET && config.FB_PAGE_TOKEN) {
            let frontAppFBHelper = {}
            if (config.FRONTAPP_API_URL && config.FRONTAPP_TOKEN && config.FRONTAPP_FB_INBOX_ID) {
                frontAppFBHelper = new FrontApp(
                    config.FRONTAPP_API_URL, 
                    config.FRONTAPP_TOKEN, 
                    config.FRONTAPP_FB_INBOX_ID
                )
            }
    
            facebookHelper = new FacebookHelper(
                config.FB_MESSAGING_API_URL, 
                config.FB_APP_SECRET, 
                config.FB_PAGE_TOKEN, 
                frontAppFBHelper,
                config.FIRST_MESSAGE_DELAY,
                config.NEXT_MESSAGE_DELAY
            )
        }
    
        let lineHelper
        if (config.LINE_TOKEN && config.LINE_SECRET) {
            let lineConfig = {
                channelAccessToken: config.LINE_TOKEN,
                channelSecret: config.LINE_SECRET,
            }
            let frontAppLINEHelper = {}
            if (config.FRONTAPP_API_URL && config.FRONTAPP_TOKEN && config.FRONTAPP_LINE_INBOX_ID) {
                frontAppLINEHelper = new FrontApp(
                    config.FRONTAPP_API_URL, 
                    config.FRONTAPP_TOKEN, 
                    config.FRONTAPP_LINE_INBOX_ID
                )
            }            
            lineHelper = new LineHelper(
                new Line.Client(lineConfig),
                Line.middleware(lineConfig),
                frontAppLINEHelper,
                config.FIRST_MESSAGE_DELAY,
                config.NEXT_MESSAGE_DELAY
            )
        } else {
            lineHelper = new LineHelper(null, function () {})
        }        

        
        let job = new FollowUpJob(
            facebookHelper, 
            lineHelper,
            JSON.parse(config.CHAT_FOLLOW_UP),
            config.FOLLOW_UP_MIN_HOUR
        )
        return job.run()        

    })
    .then(() => console.log('Done'))
    .catch(console.error)
}