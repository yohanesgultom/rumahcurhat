'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Sessions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      channel: {
        type: Sequelize.STRING
      },
      userId: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      seq: {
        type: Sequelize.INTEGER
      },
      age: {
        type: Sequelize.STRING
      },
      location: {
        type: Sequelize.STRING
      },
      conversationId: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.TINYINT,
        defaultValue: 0,
      },
      followUpStatus: {
        type: Sequelize.TINYINT,
        defaultValue: 0,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
    .then(() => {
      return queryInterface.addIndex(
        'Sessions',
        ['userId',],
        {
          indexName: 'SessionUserIdUnique',
          indicesType: 'UNIQUE'
        }
      )
    })    
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface
      .dropTable('Sessions')
      .queryInterface.removeIndex('Sessions', 'SessionUserIdUnique')
  }
}