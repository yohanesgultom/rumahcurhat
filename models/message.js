'use strict'
module.exports = (sequelize, DataTypes) => {
  const Message = sequelize.define('Message', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    sessionId: DataTypes.BIGINT,
    text: DataTypes.TEXT,
    replyTo: DataTypes.TEXT,
    seq: DataTypes.INTEGER,
    chat: DataTypes.TEXT,
    updatedAt: DataTypes.DATE,
    createdAt: DataTypes.DATE,
  }, {})
  Message.associate = function(models) {
    Message.belongsTo(models.Session, {
      foreignKey: 'sessionId',
      as: 'session',
    })
  }
  return Message
}