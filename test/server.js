'use strict'

const chai = require('chai'),
    chaiHttp = require('chai-http'),
    crypto = require('crypto'),
    sleep = require('util').promisify(setTimeout),
    chat = require('../chat.json'),
    chatFollowUp = require('../chat-follow-up.json'),
    models = require('../models/index'),
    FacebookHelper = require('../facebook'),  
    Server = require('../index'),  
    config = {
        PORT: 1337,
        FB_MESSAGING_API_URL: 'FB_MESSAGING_API_URL',
        FB_APP_SECRET: 'FB_APP_SECRET',
        FB_VERIFY_TOKEN: 'FB_VERIFY_TOKEN',
        FB_PAGE_TOKEN: 'FB_PAGE_TOKEN',
        FRONTAPP_URL: 'FRONTAPP_URL',
        FRONTAPP_TOKEN: 'FRONTAPP_TOKEN',
        CHAT: JSON.stringify(chat),
        CHAT_FOLLOW_UP: JSON.stringify(chatFollowUp),
        FOLLOW_UP_MIN_HOUR: 24,
    }

// setup chai
chai.use(chaiHttp)
const should = chai.should()

// dummy helpers
class FakeFBHelper {
    constructor(helper) {
        this.lastSenderId = null
        this.lastChat = null
        this.helper = helper
        this.frontApp = {}
    }
    getSignature (buf) {
        return this.helper.getSignature(buf)
    }
    async sendChat(senderId, chat) { 
        this.lastSenderId = senderId
        this.lastChat = chat 
        await Promise.resolve()
    }   
}

class FakeLineHelper {
    constructor(client, middleware, frontApp) {
        this.client = client
        this.middleware = middleware
        this.frontApp = frontApp
        this.lastSenderId = null
        this.replyToken = null
        this.lastChat = null
    }
    
    async sendChat(senderId, chat) { 
        this.lastSenderId = senderId
        this.lastChat = chat 
        await Promise.resolve()
    }

    // get signature
    getSignature(buf) {
        const hmac = crypto.createHmac('SHA256', this.client.config.channelSecret)
        hmac.update(buf, 'utf8')
        return hmac.digest('base64')
    }
}

class FakeLineClient {
    constructor(config) {
        this.config = config || {channelSecret: 'LINE_SECRET', channelAccessToken: 'LINE_TOKEN'} 
    }

    async replyMessage(replyToken, msg) {
        return Promise.resolve({replyToken: replyToken, msg: msg})
    }
}

const fakeLineMiddleware = (req, res, next) => {
    // imitate actual middleware that parse the body
    const body = req.body
    const strBody = Buffer.isBuffer(body) ? body.toString() : body
    req.body = JSON.parse(strBody)
    next()    
}

// server
const fbHelper = new FacebookHelper(config.FB_MESSAGING_API_URL, config.FB_APP_SECRET, config.FB_PAGE_TOKEN),
    fakeFbHelper = new FakeFBHelper(fbHelper), // reusing some real helper methods
    fakeLineHelper = new FakeLineHelper(new FakeLineClient(), fakeLineMiddleware),
    server = new Server(config, fakeFbHelper, fakeLineHelper),
    app = server.app


describe('Server', () => {
    
    beforeEach(async () => {
        await models.sequelize.drop()
        await models.migrate()
    })
    
    describe('index', () => {
        it('should not require signature', async () => {
            let res = await chai.request(app).get('/')
            res.should.have.status(200)
        })
    })

    describe('reloadConfig', () => {
        it('should reload config', (done) => {
            let now = new Date()
            let chat = {
                "1": {
                    group: "TESTBOT",
                    messages: ["Hello World"],
                    options: { "Hello": 0, "I am not World": 0},
                    type: "OPTIONS"
                }
            }
            let data = [
                {key: 'FB_PAGE_TOKEN', value: 'TEST_VALUE', createdAt: now, updatedAt: now},
                {key: 'CHAT', value: JSON.stringify(chat), createdAt: now, updatedAt: now},
            ]
            models.Config.bulkCreate(data)
            .then(() => {
                chai.request(app).post('/reloadConfig')
                .end((err, res) => {
                    res.should.have.status(200)
                    server.bot.messages.should.deep.equals(chat)
                    server.facebookHelper.pageToken.should.equals(data[0].value)
                    done()
                })        
            })
            .catch(done)
        })
    })

    describe('webhook', () => {    

        it('should allow verification without signature', (done) => {
            const challenge = 'CHALLENGE'
            chai.request(app)
            .get('/webhook')
            .query({'hub.verify_token': config.FB_VERIFY_TOKEN, 'hub.mode': 'subscribe', 'hub.challenge': challenge})
            .end((err, res) => {
                res.should.have.status(200)
                res.text.should.equals(challenge)
                done()
            })
        })
        
        it('should be able to verify', (done) => {
            const challenge = 'CHALLENGE'
            const sig = fbHelper.getSignature(Buffer.from('')) // empty body
            chai.request(app)
            .get('/webhook')
            .query({'hub.verify_token': config.FB_VERIFY_TOKEN, 'hub.mode': 'subscribe', 'hub.challenge': challenge})
            .set('x-hub-signature', sig)
            .end((err, res) => {
                res.should.have.status(200)
                res.text.should.equals(challenge)
                done()
            })
        })

        it('should reject post request without valid signature', (done) => {
            const senderId = 'SENDER_ID'
            const data = { object: 'page', entry: [{messaging: [{sender: {id: senderId}, message: {text: 'halo 😃'}}]}] }
            chai.request(app)
            .post('/webhook')
            .send(data)
            .end((err, res) => {
                res.should.have.status(400)
                done()
            }) 
        })        
        
        it('should be able to reply message', async () => {
            const senderId = 'SENDER_ID'
            const data = { object: 'page', entry: [{messaging: [{sender: {id: senderId}, message: {text: 'halo 😃'}}]}] }
            const sig = fakeFbHelper.getSignature(Buffer.from(JSON.stringify(data), 'utf-8'))
            let req = chai.request(app).keepOpen()
            let res = await req.post('/webhook')
                .send(data)
                .set('x-hub-signature', sig)
            res.should.have.status(200)
            res.text.should.equals('EVENT_RECEIVED')
            // endpoint returns response before completing the whole process
            // so need to wait a bit until it is actually complete
            // or else the database sync won't be finished yet
            await sleep(20) 
            fakeFbHelper.lastSenderId.should.equals(senderId)
            fakeFbHelper.lastChat.messages.length.should.be.above(0)
            req.close()
        })

        it('should not reply to same message twice', async () => {
            const senderId = 'SENDER_ID'
            const data = { object: 'page', entry: [{messaging: [{sender: {id: senderId}, message: {text: 'halo 😃'}}]}] }
            const sig = fakeFbHelper.getSignature(Buffer.from(JSON.stringify(data), 'utf-8'))

            // reset handledRequestIds
            server.handledRequestIds = {FB: [], LINE: []}

            let req = chai.request(app).keepOpen()
            let res = await req.post('/webhook')
                .send(data)
                .set('x-hub-signature', sig)
            res.should.have.status(200)      
            // endpoint returns response before completing the whole process
            // so need to wait a bit until it is actually complete
            // or else the database sync won't be finished yet
            await sleep(10)       
            fakeFbHelper.lastChat.messages.length.should.be.above(0)            

            // should not respond same message
            fakeFbHelper.lastChat = null
            res = await req.post('/webhook')
                .send(data)
                .set('x-hub-signature', sig)
            res.should.have.status(200) 
            // endpoint returns response before completing the whole process
            // so need to wait a bit until it is actually complete
            // or else the database sync won't be finished yet
            await sleep(10)            
            should.not.exist(fakeFbHelper.lastChat)
            req.close()
        })        

    })
  
    describe('callback', () => {

        it('should success', async () => {
            const data = {events: [{
                replyToken: 'REPLY_TOKEN', 
                source: {userId: 'LINE_USER_ID'}, 
                message: {text: 'Hi'},
            }]}
            const sig = fakeLineHelper.getSignature(Buffer.from(JSON.stringify(data), 'utf-8'))
            let res = await chai.request(app)
                .post('/callback')
                .send(data)
                .set('x-line-signature', sig)
            res.should.have.status(200)
            res.text.should.equals('EVENT_RECEIVED') 
            // endpoint returns response before completing the whole process
            // so need to wait a bit until it is actually complete 
            // or else the database sync won't be finished yet 
            await sleep(10)         
        })

        it('should not reply to same message twice', async () => {
            const data = {
                events:[
                    {
                        type:"message",
                        replyToken: "cc72d5d77b804d72b9027d95d72fe9de",
                        source: {userId:"Uefdbeaabceed7889a616cd0963c79c47", type:"user"},
                        timestamp: 1530006066209,
                        message: {type:"text", id:"8172375207541", text:"Testing line webhook 02"}
                    }
                ]
            }
            const sig = fakeLineHelper.getSignature(Buffer.from(JSON.stringify(data), 'utf-8'))

            // reset handledRequestIds
            server.handledRequestIds = {FB: [], LINE: []}

            let req = chai.request(app).keepOpen()
            let res = await req.post('/callback')
                .send(data)
                .set('x-line-signature', sig)
            res.should.have.status(200)      
            // endpoint returns response before completing the whole process
            // so need to wait a bit until it is actually complete
            // or else the database sync won't be finished yet
            await sleep(10)       
            fakeLineHelper.lastChat.messages.length.should.be.above(0)            

            // should not respond same message
            fakeLineHelper.lastChat = null
            res = await req.post('/callback')
                .send(data)
                .set('x-line-signature', sig)
            res.should.have.status(200) 
            // endpoint returns response before completing the whole process
            // so need to wait a bit until it is actually complete
            // or else the database sync won't be finished yet
            await sleep(10)
            should.not.exist(fakeLineHelper.lastChat)
            req.close()
        })

    })
})
