/**
 * FrontApp API helper
 */

'use strict'

const request = require('request')

class FrontApp {
    
    constructor(apiUrl, apiToken, inboxId) {
        this.apiUrl = apiUrl
        this.apiToken = apiToken
        this.inboxId = inboxId
    }
    
    async call(method, path, data) {
        let headers = {'Authorization': 'Bearer ' + this.apiToken}
        let url = this.apiUrl + path
        let options = {
            method: method,
            url: url,
            headers: headers,
            json: true,
        }
        if (method === 'get') {
            options.qs = data
        } else {
            options.body = data
        }
        return new Promise((resolve, reject) => {
            request(options, (error, res, body) => {
                if (error) {
                    reject(error)
                } else {
                    resolve(body)
                }
            })  
        })
    }

    async findConversationByHandle(handle, type, status, maxPages) {
        status = status || 'unassigned'
        maxPages = maxPages || 1

        let result
        let pageCount = 0
        let nextPage
        do {
            let data = await this.call('get', `/inboxes/${this.inboxId}/conversations`)
            nextPage = data._pagination.next
            for (let i = 0; i < data._results.length; i++) {
                let c = data._results[i]
                if (c.last_message.type === type && c.recipient.handle === handle) {
                    result = c
                    break
                }
            }            
        } while (!result && pageCount < maxPages && nextPage)
        return result
    }

    async tagConversation(convId, tags) {
        return this.call('patch', '/conversations/' + convId, {tags: tags})
    }
}

module.exports = FrontApp
