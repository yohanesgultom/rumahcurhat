/**
 * LINE Messaging API helper
 */

'use strict'

const models = require('./models/index'),
    sleep = require('util').promisify(setTimeout)

class LineHelper {
    
    constructor(client, middleware, frontApp, firstMessageDelay, nextMessageDelay) {  
        this.client = client
        this.middleware = middleware
        this.frontApp = frontApp
        this.firstMessageDelay = firstMessageDelay || 0
        this.nextMessageDelay = nextMessageDelay || 0
    }

    // send bot chat object
    async sendChat(senderId, chat) {
        if (!chat) {
            // do nothing
        } else if (chat.type === 'OPTIONS') {                            
            // send messages
            let messages = this.buildOptionQuickReplyMessages(chat)
            for (let i = 0; i < messages.length; i++) {
                await sleep(i > 0 ? this.nextMessageDelay : this.firstMessageDelay)
                await this.client.pushMessage(senderId, messages[i])                
            }

        } else if (chat.type === 'HANDOVER') {     
            // assuming conversation.recipient.handle == LINE message sender id
            let conv = await this.frontApp.findConversationByHandle(senderId, 'custom')
            if (conv) {
                let tags = conv.tags.map(t => t.name)
                tags.push(chat.frontapp_tag)
                // update tags
                await this.frontApp.tagConversation(conv.id, tags)
                // update conversationId
                let s = await models.Session.findOne({
                    where:{userId: senderId, channel: 'LINE'},
                    order: [['id', 'DESC']],
                })
                if (s) {
                    s.conversationId = conv.id
                    await s.save()
                }
            }
        } else {
            // send plain messages
            for (let i = 0; i < chat.messages.length; i++) {
                await sleep(i > 0 ? this.nextMessageDelay : this.firstMessageDelay)
                await this.client.pushMessage(senderId, {
                    type: 'text', 
                    text: chat.messages[i],
                })
            }
            
        }
    }

    limit(str, max, suffix) {
        suffix = suffix || '..'
        return str.length > max ? str.substr(0, (max-suffix.length)) + suffix : str
    } 

    buildOptionQuickReplyMessages(chat) {
        // https://developers.line.biz/en/reference/messaging-api/#message-action
        const MAX_OPT_LEN = 20
        let result = []
        let messages = chat.messages.slice(0) // copy array
        let lastMesage = messages.pop()

        // build messages
        for (let i = 0; i < messages.length; i++) {
            result.push({
                type: 'text', 
                text: chat.messages[i],
            })
        }

        // options message
        let optionsArr = []
        let items = []
        let longest = 0
        for (let opt in chat.options) {
            longest = opt.length > longest ? opt.length : longest
            optionsArr.push(`* ${opt}`)
            items.push({
                type: 'action', 
                action: { 
                    type: 'message',
                    label: this.limit(opt, MAX_OPT_LEN),
                    text: opt,
                }
            })
        }

        // below max, send last message along
        // with quick reply options
        if (longest <= MAX_OPT_LEN) {
            result.push({
                type: 'text', 
                text: lastMesage,
                quickReply: { items: items }
            })    
        }

        // equal or above max, send options list as message
        // along with quick reply options
        else {
            result.push({type: 'text', text: lastMesage})
            result.push({
                type: 'text', 
                text: optionsArr.join('\n'),
                quickReply: { items: items }
            })    
        }

        return result
    }
    
}

module.exports = LineHelper