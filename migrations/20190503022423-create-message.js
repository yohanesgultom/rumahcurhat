'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Messages', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      sessionId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'Sessions',
            key: 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade',
      },
      text: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      replyTo: {
        type: Sequelize.TEXT
      },
      seq: {
        type: Sequelize.INTEGER
      },
      chat: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Messages')
  }
}