'use strict';

// read from config.json
const defaultConfig = require('../config.json')
const defaultChat = require('../chat.json')
const defaultChatFollowUp = require('../chat-follow-up.json')

module.exports = {
  up: (queryInterface, Sequelize) => {
    let data = []
    // ignore database
    delete defaultConfig.DATABASE
    let now = new Date()
    for (let key in defaultConfig) {
      data.push({key: key, value: defaultConfig[key], createdAt: now, updatedAt: now})
    }
    // add default chat messages
    data.push({key: 'CHAT', value: JSON.stringify(defaultChat), createdAt: now, updatedAt: now})
    data.push({key: 'CHAT_FOLLOW_UP', value: JSON.stringify(defaultChatFollowUp), createdAt: now, updatedAt: now})
    
    return queryInterface.bulkInsert('Configs', data, {})
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Configs', null, {})
  }
};
