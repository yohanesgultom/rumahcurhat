/**
 * Facebook Messaging API helper
 */

'use strict'

const crypto = require('crypto'),
    request = require('request'),
    models = require('./models/index'),
    sleep = require('util').promisify(setTimeout)

class FacebookHelper {
    
    constructor(apiURL, appSecret, pageToken, frontApp, firstMessageDelay, nextMessageDelay) {  
        this.apiURL = apiURL
        this.appSecret = appSecret
        this.pageToken = pageToken
        this.frontApp = frontApp
        this.firstMessageDelay = firstMessageDelay || 0
        this.nextMessageDelay = nextMessageDelay || 0
    }
    
    // get signature from buffer
    getSignature(buf) {
        const hmac = crypto.createHmac('sha1', this.appSecret)
        hmac.update(buf, 'utf8')
        return `sha1=${hmac.digest('hex')}`
    }
    
    // send facebook message object
    sendMessage(data) {
        return new Promise((resolve, reject) => {
            request.post({
                uri: `${this.apiURL}?access_token=${this.pageToken}`,
                json: true,
                body: data,
            }, (error, res, body) => {
                if (error) {
                    reject(error)
                } else {
                    if (body.error) {
                        reject(body.error)
                    } else {
                        resolve(body)
                    }              
                }
            })  
        })
    }
    
    // send bot chat object
    async sendChat(senderId, chat) {
        if (!chat) {
            // do nothing
        } else if (chat.type === 'OPTIONS') {                            
            // send messages
            let messages = this.buildOptionQuickReplyMessages(senderId, chat)
            // let messages = this.buildOptionButtonMessages(senderId, chat)
            for (let i = 0; i < messages.length; i++) {
                await sleep(i > 0 ? this.nextMessageDelay : this.firstMessageDelay)
                await this.sendMessage(messages[i])
            }

        } else if (chat.type === 'HANDOVER') {            
            // assuming conversation.recipient.handle == facebook message sender id
            let conv = await this.frontApp.findConversationByHandle(senderId, 'facebook')            
            if (conv) {
                let tags = conv.tags.map(t => t.name)
                tags.push(chat.frontapp_tag)
                // update tags
                await this.frontApp.tagConversation(conv.id, tags)
                // update conversationId
                let s = await models.Session.findOne({
                    where:{userId: senderId, channel: 'FB'},
                    order: [['id', 'DESC']],
                })
                if (s) {
                    s.conversationId = conv.id
                    await s.save()
                }
            }
            
        } else {
            // send plain messages
            for (let i = 0; i < chat.messages.length; i++) {
                await sleep(i > 0 ? this.nextMessageDelay : this.firstMessageDelay)
                await this.sendMessage({
                    messaging_type: 'RESPONSE',
                    recipient: {id: senderId},
                    message: {text: chat.messages[i], metadata: senderId},
                })            
            }
            
        }
    }

    buildOptionButtonMessages(senderId, chat) {
        let result = []

        // build elements
        const maxButtons = 3 // facebook limitation
        let elements = []
        for (let opt in chat.options) {
            let pageCount = elements.length
            if (pageCount <= 0 || elements[pageCount-1].buttons.length >= maxButtons) {
                elements.push({
                    title: `Page ${pageCount+1}`,
                    buttons: [],
                })
            }
            // keep adding buttons until max
            let lastId = elements.length - 1
            elements[lastId].buttons.push({
                type: 'postback', 
                title: opt, 
                payload: opt,
            })
        }

        // build messages
        for (let i = 0; i < chat.messages.length; i++) {
            result.push({
                messaging_type: 'RESPONSE',
                recipient: {id: senderId},
                message: {text: chat.messages[i]},
            })
        }

        // last message
        result.push({
            messaging_type: 'RESPONSE',
            recipient: {id: senderId},
            message: {
                attachment: {
                    type: 'template',
                    payload: { template_type: 'generic', elements: elements }
                }                
            },
        })        

        return result
    }

    buildOptionQuickReplyMessages(senderId, chat) {
        // https://developers.facebook.com/docs/messenger-platform/reference/send-api/quick-replies/
        const MAX_OPT_LEN = 20
        let result = []
        let messages = chat.messages.slice(0)    
        let lastMesage = messages.pop()

        // build messages
        for (let i = 0; i < messages.length; i++) {
            result.push({
                messaging_type: 'RESPONSE',
                recipient: {id: senderId},
                message: {text: chat.messages[i]},
            })
        }

        let quickReplies = []
        let optionsArr = []
        let longest = 0
        for (let opt in chat.options) {
            longest = opt.length > longest ? opt.length : longest
            optionsArr.push(`* ${opt}`)
            quickReplies.push({
                content_type: 'text',
                title: opt,
                payload: opt,
            })
        }
        
        // below max, send last message along
        // with quick reply options
        if (longest <= MAX_OPT_LEN) {
            result.push({
                messaging_type: 'RESPONSE',
                recipient: {id: senderId},
                message: {text: lastMesage, quick_replies: quickReplies},
            })
        } 
        
        // equal or above max, send options list as message
        // along with quick reply options        
        else {
            result.push({
                messaging_type: 'RESPONSE',
                recipient: {id: senderId},
                message: {text: lastMesage},
            })
            result.push({
                messaging_type: 'RESPONSE',
                recipient: {id: senderId},
                message: {
                    text: optionsArr.join('\n'), 
                    quick_replies: quickReplies
                },
            })
        }
        return result
    }

    
}

module.exports = FacebookHelper