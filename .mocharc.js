'use strict';

module.exports = {
  diff: true,
  extension: ['js'],
  require: ['./test/mocha.env.js'],
  package: './package.json',
  timeout: 20000,
  exit: true,
};