'''
Data processing scripts
'''

import json
import openpyxl
import sys
import re


def convert_json_list_to_object(input_path, output_path, key_name='seq'):
    '''
    Convert JSON list to JSON object to speed up look up process
    '''
    with open(input_path, 'r') as i, open(output_path, 'w') as o:
        data = {}
        l = json.load(i)
        for d in l:
            seq = d[key_name]
            del d[key_name]
            if d['type'] == 'OPTIONS':
                for opt in d['options']:
                    options[opt['text']] = opt['next_seq']
                d['options'] = options
            data[seq] = d
        json.dump(data, o, indent=4)


def read_jesus_connect_excel(xls_file, count=0):
    '''
    Convert JesusConnect.xlsx to JSON object
    '''
    wb = openpyxl.load_workbook(xls_file)
    sheet = wb.get_sheet_by_name(wb.get_sheet_names()[0])

    # read data
    data = {}
    sequence_map = {
        'START BOT AGAIN': 1,
    }
    left_top = 'A2'
    right_bottom = 'D142'
    # nonalphanum = re.compile('[\W]+')
    nonascii = re.compile('[^\x00-\x7F]')
    next_code_set = {
        'HAVE YOU CONSIDERED?'
    }
    for cells in sheet[left_top:right_bottom]:
        if cells[0].value is not None:
            code = nonascii.sub('', cells[0].value).strip().upper()
            messages = list(filter(bool, cells[1].value.split('\n')))
            answers = list(filter(bool, cells[3].value.split('\n')))
            next_codes = list(filter(bool, cells[2].value.split('\n')))
            options = [] 
            for i in range(len(answers)):
                next_code = nonascii.sub('', next_codes[i]).strip().upper()        
                options.append({'text': answers[i], 'next_code': next_code})
                next_code_set.add(next_code)

            data_tmp = {
                'code': code,
                'group': 'JESUSBOT', 
            }
            if len(messages) > 0:
                data_tmp['messages'] = messages

            if len(options) > 0:
                data_tmp["type"] = 'OPTIONS'
                data_tmp["options"] = options
            else:
                data_tmp["type"] = 'TRANSITION'
            
            data[count] = data_tmp
            sequence_map[code] = count
            count = count + 1
        
    # populate next_seq
    for key, d in data.items():        
        new_options = {}
        for o in d['options']:
            next_seq = sequence_map[o['next_code']] if o['next_code'] in sequence_map else -1
            # o['next_seq'] = next_seq
            new_options[o['text']] = next_seq
        d['options'] = new_options
        # del code for consistency
        del d['code']

    with open('out.json', 'w') as f:
        json.dump(data, f, indent=4)

    # with open('codes.txt', 'w') as f:
    #     f.write('\n'.join(sorted(sequence_map.keys())))

    # with open('next_code_set.txt', 'w') as f:      
    #     f.write('\n'.join(sorted(next_code_set)))

def sort_json_object(input_path, output_path):
    '''
    Lexicographically sort JSON object by keys
    '''
    with open(input_path, 'r') as i, open(output_path, 'w') as o:
        data = json.load(i)
        json.dump(data, o, sort_keys=True, indent=4)

if __name__ == '__main__':
    # convert_json_list_to_object(sys.argv[1], sys.argv[2])
    # read_jesus_connect_excel('JesusConnect.xlsx', 25)
    # sort_json_object(sys.argv[1], sys.argv[2])

