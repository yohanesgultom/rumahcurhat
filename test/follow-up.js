'use strict'

const chai = require('chai'),
    crypto = require('crypto'),
    models = require('../models/index'),
    chat = require('../chat.json'),
    chatFollowUp = require('../chat-follow-up.json'),
    FacebookHelper = require('../facebook'),  
    FollowUpJob = require('../follow-up'),
    config = {
        FB_MESSAGING_API_URL: 'FB_MESSAGING_API_URL',
        FB_APP_SECRET: 'FB_APP_SECRET',
        FB_VERIFY_TOKEN: 'FB_VERIFY_TOKEN',
        FB_PAGE_TOKEN: 'FB_PAGE_TOKEN',
        FRONTAPP_URL: 'FRONTAPP_URL',
        FRONTAPP_TOKEN: 'FRONTAPP_TOKEN',
        CHAT: JSON.stringify(chat),
        CHAT_FOLLOW_UP: JSON.stringify(chatFollowUp),
        FOLLOW_UP_MIN_HOUR: 24,
    }

// dummy helpers
class FakeFBHelper {
    constructor(helper) {
        this.lastSenderId = null
        this.lastChat = null
        this.helper = helper
        this.frontApp = {}
        this.sendCount = 0
    }
    getSignature (buf) {
        return this.helper.getSignature(buf)
    }
    async sendChat(senderId, chat) { 
        this.lastSenderId = senderId
        this.lastChat = chat 
        this.sendCount++
        await Promise.resolve()
    }   
}

class FakeLineHelper {
    constructor(client, middleware, frontApp) {
        this.client = client
        this.middleware = middleware
        this.frontApp = frontApp
        this.lastSenderId = null
        this.replyToken = null
        this.lastChat = null
        this.sendCount = 0
    }
    
    async sendChat(senderId, chat) { 
        this.lastSenderId = senderId
        this.lastChat = chat 
        this.sendCount++
        await Promise.resolve()
    }

    // get signature
    getSignature(buf) {
        const hmac = crypto.createHmac('SHA256', this.client.config.channelSecret)
        hmac.update(buf, 'utf8')
        return hmac.digest('base64')
    }
}

class FakeLineClient {
    constructor(config) {
        this.config = config || {channelSecret: 'LINE_SECRET', channelAccessToken: 'LINE_TOKEN'} 
    }

    async replyMessage(replyToken, msg) {
        return Promise.resolve({replyToken: replyToken, msg: msg})
    }
}

const fakeLineMiddleware = (req, res, next) => {
    // imitate actual middleware that parse the body
    const body = req.body
    const strBody = Buffer.isBuffer(body) ? body.toString() : body
    req.body = JSON.parse(strBody)
    next()    
}

// helpers
const fbHelper = new FacebookHelper(config.FB_MESSAGING_API_URL, config.FB_APP_SECRET, config.FB_PAGE_TOKEN),
    fakeFbHelper = new FakeFBHelper(fbHelper), // reusing some real helper methods
    fakeLineHelper = new FakeLineHelper(new FakeLineClient(), fakeLineMiddleware)


describe('FollowUpJob', () => {
    
    beforeEach(async () => {
        await models.sequelize.drop()
        await models.migrate()
    })
    
    it('getMinUpdatedAt()', async () => {
        let minUpdateHour = 24
        let now = new Date()
        let job = new FollowUpJob(
            fakeFbHelper, 
            fakeLineHelper,
            config.CHAT_FOLLOW_UP,
            minUpdateHour,
            now
        )
        let delta = (now.getTime() - job.getMinUpdatedAt().getTime()) / 3600000
        delta.should.equals(minUpdateHour)
    })

    it('run()', async () => {
        let minUpdateHour = 24
        let expectedFollowUpFB = 15
        let expectedFollowUpLINE = 12
        let expectedDoNotFollowUpHandovered = 10
        let expectedDoNotFollowUpNew = 7
        let sessions = []

        // need follow up        
        for (let i = 0; i < expectedFollowUpFB; i++) {
            sessions.push({
                userId: `NEED_FOLLOW_UP_FB_${i}`,
                channel: 'FB',
                seq: Math.floor(Math.random() * 50),
                age: i + 17,
                location: `LOCATION_${i}`,
            })
        }
        for (let i = 0; i < expectedFollowUpLINE; i++) {
            sessions.push({
                userId: `NEED_FOLLOW_UP_LINE_${i}`,
                channel: 'LINE',
                seq: Math.floor(Math.random() * 50),
                age: i + 17,
                location: `LOCATION_${i}`,
            })
        }
        // does not need follow up        
        for (let i = 0; i < expectedDoNotFollowUpHandovered; i++) {
            sessions.push({
                userId: `DO_NOT_NEED_FOLLOW_UP_HANDOVERED_${i}`,
                channel: Math.random() > 0.5 ? 'LINE' : 'FB',
                seq: Math.floor(Math.random() * 50),
                age: i + 17,
                location: `LOCATION_${i}`,
                status: models.Session.StatusCode.HANDOVERED,
            })
        }
        for (let i = 0; i < expectedDoNotFollowUpNew; i++) {
            sessions.push({
                userId: `DO_NOT_NEED_FOLLOW_UP_NEW_${i}`,
                channel: Math.random() > 0.5 ? 'LINE' : 'FB',
                seq: Math.floor(Math.random() * 50),
                age: i + 17,
                location: `LOCATION_${i}`,
            })
        }        
        await models.Session.bulkCreate(sessions)
        let recently = new Date(new Date().getTime() + (minUpdateHour - 1) * 3600000)
        let recentlyStr = recently.toJSON().slice(0, 19).replace('T', ' ')
        await models.sequelize.query(`UPDATE Sessions SET updatedAt = '${recentlyStr}' WHERE userId LIKE 'DO_NOT_NEED_FOLLOW_UP_NEW_%'`)

        // init job       
        let tommorow = new Date(new Date().getTime() + minUpdateHour * 3600000)
        let job = new FollowUpJob(
            fakeFbHelper, 
            fakeLineHelper,
            JSON.parse(config.CHAT_FOLLOW_UP),
            minUpdateHour,
            tommorow
        )
        await job.run()         
        
        fakeFbHelper.sendCount.should.equals(expectedFollowUpFB)
        fakeLineHelper.sendCount.should.equals(expectedFollowUpLINE)    
        let count = await models.Session.count({where: {followUpStatus: models.Session.FollowUpStatusCode.IN_PROGRESS}})
        count.should.equals(expectedFollowUpFB + expectedFollowUpLINE)
    })
})
