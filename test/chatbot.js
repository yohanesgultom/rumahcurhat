'use strict'

const assert = require('assert'),
	ChatBot = require('../chatbot'),
	models = require('../models/index'),
	messages = require('../chat.json'),
	followUpMessages = require('../chat-follow-up.json'),
	bot = new ChatBot(messages, followUpMessages)

describe('Chatbot', () => {
	
	beforeEach(async () => {
		await models.sequelize.sync({ force: true, })
	})

	describe('chat()', () => {    
		
		it('should return initial message for the first time', async () => {
			let event = {user_id: 'test_id', message: 'hello'}
			let reply = await bot.chat(event.user_id, event.message, 'TEST')
			let session = await models.Session.findOne({where: {userId: event.user_id}})
			let messages = await session.getMessages()
			assert.equal(messages.length, 1)
			assert.equal(messages[0].text, event.message)
			assert.equal(messages[0].seq, null)
			assert.equal(messages[0].replyTo, null)
			assert.equal(messages[0].chat, null)
			assert.equal(session.seq, 1)
			assert.deepEqual(reply, bot.messages[1])
			assert.equal(session.status, models.Session.StatusCode.NEW)
		})

		it('should go to next sequence based on answer', async () => {
			let events = [
				{user_id: 'test_id', message: 'hello'},
				{user_id: 'test_id', message: 'Ya'},
			]
			let reply = null
			for (let i=0; i < events.length; i++) {
				let e = events[i]
				reply = await bot.chat(e.user_id, e.message, 'TEST')
			}      
			let session = await models.Session.findOne({where: {userId: events[0].user_id}})
			let messages = await session.getMessages()
			assert.equal(session.seq, 2)
			assert.deepEqual(reply, bot.messages[2])
			assert.equal(messages.length, events.length)
			assert.equal(messages[1].seq, 1)
			assert.deepEqual(JSON.parse(messages[1].replyTo), bot.messages[1].messages)
			assert.deepEqual(JSON.parse(messages[1].chat), bot.messages[1])      
		})
		
		it('should store variable', async () => {
			let event = {user_id: 'test_id', message: '17 tahun'}
			let reply = await bot.chat(event.user_id, event.message, 'TEST', 2)
			let session = await models.Session.findOne({where: {userId: event.user_id}})
			assert.equal(session.age, event.message)
			assert.equal(session.seq, 3)
			assert.deepEqual(reply, bot.messages[3])
		})

		it('should handle transition', async () => {			
			let event = {user_id: 'test_id', message: 'Sedih'}
			let transition = bot.getMessage(bot.messages[5].options[event.message])
			let nextSeq = transition.next_seq
			let reply = await bot.chat(event.user_id, event.message, 'TEST', 5)
			let session = await models.Session.findOne({where: {userId: event.user_id}})
			assert.equal(session.seq, nextSeq)
			assert.deepEqual(reply.messages, [
				'Setiap kesedihan yang kamu rasain waktu ngadepin masalah pasti ada jalan keluar',
				'Apa yang membuatmu merasa seperti itu?',
			])

			await models.Session.update({seq: 5}, {where: {id: session.id}})
			reply = await bot.chat(event.user_id, 'Marah', 'TEST')
			assert.deepEqual(reply.messages, [
				'Marah adalah cara untuk mengekspresikan emosi yang kuat. Kamu pengen marah? Kesel Banget? Semua orang waktu ngalami masalah biasanya mudah untuk marah',
				'Apa yang membuatmu merasa seperti itu?',
			])

			await models.Session.update({seq: 5}, {where: {id: session.id}})
			reply = await bot.chat(event.user_id, 'Frustrasi', 'TEST')
			assert.deepEqual(reply.messages, [
				'Wajar kok kalau kamu ngerasa Frustasi dan ngga cuma kamu yang ngalamin kalau kamu lagi ada masalah',
				'Apa yang membuatmu merasa seperti itu?',
			])
		})

		it('should be able to save emoji', async () => {
			let event = {user_id: 'test_id', message: "Dia Tuhanku \ud83d\udca5"}
			let reply = await bot.chat(event.user_id, event.message, 'TEST', 26)
			let session = await models.Session.findOne({where: {userId: event.user_id}})
			let messages = await session.getMessages()
			assert.equal(messages.length, 1)
			assert.equal(session.seq, 27)
			assert.deepEqual(reply, bot.messages[27])
		})

		it('should return null if next sequence not exists', async () => {
			let event = {user_id: 'test_id', message: "Tidak"}
			let reply = await bot.chat(event.user_id, event.message, 'TEST', 1)      
			assert.equal(reply, undefined)
		})

		it('should repeat message if unable to recognize response', async () => {
			let event = {user_id: 'test_id', message: "Lorem ipsum"}
			let reply = await bot.chat(event.user_id, event.message, 'TEST', 1)
			assert.deepEqual(reply, bot.messages[1])
		})

		it('should stop sending message after handover', async () => {
			let event = {user_id: 'test_id', message: "Ngobrol dengan tim RC"}
			let reply = await bot.chat(event.user_id, event.message, 'TEST', 22)
			let session = await models.Session.findOne({where: {userId: event.user_id}})
			assert.equal(session.status, models.Session.StatusCode.HANDOVERED)
			assert.equal(reply.type, 'HANDOVER')
			assert(reply.frontapp_tag.length > 0)
		})

		it('should restart on any state before handover', async () => {      
			let event = {user_id: 'test_id', channel: 'TEST'}
			let reply = await bot.chat(event.user_id, 'ulangi Bot', event.channel, 3)
			assert.deepEqual(reply, bot.messages[1])

			reply = await bot.chat(event.user_id, 'ulangi Bot', event.channel, 5)
			assert.deepEqual(reply, bot.messages[1])

			reply = await bot.chat(event.user_id, ' ulangI Bot ', event.channel, 11)
			assert.deepEqual(reply, bot.messages[1])      
		})

		it('should restart on restart command even after handover', async () => {      
			let event = {user_id: 'test_id', channel: 'TEST', message: "Ngobrol dengan tim RC"}
			await bot.chat(event.user_id, event.message, event.channel, 22)

			// random message should return undefined
			let reply = await bot.chat(event.user_id, 'hello', event.channel)
			assert.deepEqual(reply, undefined)

			// restart command should restart bot
			reply = await bot.chat(event.user_id, 'ulangi bot', event.channel)
			assert.deepEqual(reply, bot.messages[1])

			reply = await bot.chat(event.user_id, 'Ulangi Bot', event.channel)
			assert.deepEqual(reply, bot.messages[1])
		})
		
		it('should be able to use custom restart command', async () => {      
			let customBot = new ChatBot(messages, followUpMessages, 'hail Hydra')
			let event = {user_id: 'test_id', channel: 'TEST'}
			let reply = await customBot.chat(event.user_id, 'Hail HYDRA', event.channel, 3)
			assert.deepEqual(reply, customBot.messages[1])

			reply = await customBot.chat(event.user_id, 'hail hydra', event.channel, 5)
			assert.deepEqual(reply, customBot.messages[1])

			reply = await customBot.chat(event.user_id, 'hail Hydra  ', event.channel, 11)
			assert.deepEqual(reply, customBot.messages[1])      
		})    

		it('should allow continue last chat when following up chat', async () => {      
			let event = {user_id: 'test_id', channel: 'TEST'}
			await bot.chat(event.user_id, 'JAKARTA', event.channel, 3)
			let session = await models.Session.findOne({where: {userId: event.user_id}})
			session.followUpStatus = models.Session.FollowUpStatusCode.IN_PROGRESS
			await session.save()

			let reply = await bot.chat(event.user_id, 'Lanjutkan', event.channel)
			assert.deepEqual(reply, bot.messages[session.seq])
			await session.reload()
			assert.equal(session.followUpStatus, models.Session.FollowUpStatusCode.DONE)
		})

		it('should reply properly when following up chat', async () => {      
			let event = {user_id: 'test_id', channel: 'TEST'}
			await bot.chat(event.user_id, 'JAKARTA', event.channel, 3)
			let session = await models.Session.findOne({where: {userId: event.user_id}})
			session.followUpStatus = models.Session.FollowUpStatusCode.IN_PROGRESS
			await session.save()

			let msg = 'Tidak'
			let reply = await bot.chat(event.user_id, msg, event.channel)
			let nextSeq = followUpMessages.QUESTION.options[msg]
			assert.deepEqual(reply, bot.followUpMessages[nextSeq])
			await session.reload()
			assert.equal(session.followUpStatus, models.Session.FollowUpStatusCode.DONE)
		})		

		it('should be able to restart chat during follow up', async () => {      
			let event = {user_id: 'test_id', channel: 'TEST'}
			await bot.chat(event.user_id, 'JAKARTA', event.channel, 3)
			let session = await models.Session.findOne({where: {userId: event.user_id}})
			session.followUpStatus = models.Session.FollowUpStatusCode.IN_PROGRESS
			await session.save()

			let msg = 'Ulangi Bot'
			let reply = await bot.chat(event.user_id, msg, event.channel)
			assert.deepEqual(reply, bot.messages[1])
			await session.reload()
			assert.equal(session.followUpStatus, models.Session.FollowUpStatusCode.DONE)
		})		

		it('should be handle unkwnon response during follow up', async () => {      
			let event = {user_id: 'test_id', channel: 'TEST'}
			await bot.chat(event.user_id, 'JAKARTA', event.channel, 3)
			let session = await models.Session.findOne({where: {userId: event.user_id}})
			session.followUpStatus = models.Session.FollowUpStatusCode.IN_PROGRESS
			await session.save()

			let msg = 'Huwalakumba'
			let reply = await bot.chat(event.user_id, msg, event.channel)
			assert.equal(reply, bot.followUpMessages.QUESTION)
			await session.reload()
			assert.equal(session.followUpStatus, models.Session.FollowUpStatusCode.IN_PROGRESS)
		})			

	})
})