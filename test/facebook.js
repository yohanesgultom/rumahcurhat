'use strict'

const assert = require('assert'),
    nock = require('nock'),
    chats = require('../chat.json'),
    config = {
        FB_MESSAGING_API_URL: 'FB_MESSAGING_API_URL',
        FB_APP_SECRET: 'FB_APP_SECRET',
        FB_VERIFY_TOKEN: 'FB_VERIFY_TOKEN',
        FB_PAGE_TOKEN: 'FB_PAGE_TOKEN',
    },
    FacebookHelper = require('../facebook')

class FakeFrontApp {
    
    constructor(findConversationByHandle, tagConversation) {
        this.findConversationByHandle = findConversationByHandle
        this.tagConversation = tagConversation
    }
    
    async findConversationByHandle(handle, type, status, maxPages) {        
        return this.findConversationByHandle(handle, type, status, maxPages)
    }
    
    async tagConversation(convId, tags) {
        return this.tagConversation(convId, tags)
    }
}


describe('Facebook', () => {
    
    describe('buildOptionButtonMessages()', () => {
        
        it('should build correct paged button options messages', async () => {
            const helper = new FacebookHelper()
            const recipientId = 'RECIPIENT_ID'
            const expectedMessages = [
                {
                    messaging_type: 'RESPONSE',
                    recipient: { id: recipientId },
                    message: {text: 'Apa Masalahmu?'}
                },
                {
                    messaging_type: 'RESPONSE',
                    recipient: { id: recipientId },
                    message: {
                        attachment: {
                            type: 'template',
                            payload: {
                                template_type: 'generic',
                                elements: [
                                    {
                                        title: 'Page 1',
                                        buttons: [
                                            {type: 'postback', title: 'Hubungan dengan keluarga', payload: 'Hubungan dengan keluarga'},
                                            {type: 'postback', title: 'Masalah percintaan', payload: 'Masalah percintaan'},
                                            {type: 'postback', title: 'Tujuan Hidup', payload: 'Tujuan Hidup'},
                                        ]
                                    },
                                    {
                                        title: 'Page 2',
                                        buttons: [
                                            {type: 'postback', title: 'Lainnya', payload: 'Lainnya'},
                                        ]
                                    },
                                ]
                            }
                        }
                    }
                },
            ]
            
            let messages = helper.buildOptionButtonMessages(recipientId, chats[4])
            assert.deepEqual(messages, expectedMessages)

        })
        
    })
    
    describe('buildOptionQuickReplyMessages()', () => {

        it('should build correct quick reply options messages', async () => {
            const helper = new FacebookHelper()
            const recipientId = 'RECIPIENT_ID'
            const expectedMessages = [
                {
                    messaging_type: 'RESPONSE',
                    recipient: { id: recipientId },
                    message: {
                        text: 'Apakah kamu tau klo Tuhan sayang sama kamu?',
                        quick_replies: [
                            {content_type: 'text', title: 'Tidak', payload: 'Tidak'},
                            {content_type: 'text', title: 'Ya', payload: 'Ya'},
                        ]
                    }
                },
            ]            
            let messages = helper.buildOptionQuickReplyMessages(recipientId, chats[11])
            assert.deepEqual(messages, expectedMessages)
        })

        it('should build correct quick reply options when one or more option exceed max allowed chars', async () => {
            const helper = new FacebookHelper()
            const recipientId = 'RECIPIENT_ID'
            const expectedMessages = [
                {
                    messaging_type: 'RESPONSE',
                    recipient: { id: recipientId },
                    message: {
                        text: 'Apa Masalahmu?',
                    }
                },
                {
                    messaging_type: 'RESPONSE',
                    recipient: { id: recipientId },
                    message: {
                        text: '* Hubungan dengan keluarga\n* Masalah percintaan\n* Tujuan Hidup\n* Lainnya',
                        quick_replies: [
                            {content_type: 'text', title: 'Hubungan dengan keluarga', payload: 'Hubungan dengan keluarga'},
                            {content_type: 'text', title: 'Masalah percintaan', payload: 'Masalah percintaan'},
                            {content_type: 'text', title: 'Tujuan Hidup', payload: 'Tujuan Hidup'},
                            {content_type: 'text', title: 'Lainnya', payload: 'Lainnya'},
                        ]
                    }
                },
            ]            
            let messages = helper.buildOptionQuickReplyMessages(recipientId, chats[4])
            assert.deepEqual(messages, expectedMessages)
        })
        
    })
    
})