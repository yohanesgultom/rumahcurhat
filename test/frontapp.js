'use strict'

const assert = require('assert'),
    nock = require('nock'),
    FrontApp = require('../frontapp'),
    app = new FrontApp('https://api2.frontapp.com', 'TOKEN', 'INBOX_ID')

describe('FrontApp', () => {
    
    describe('findConversationByHandle()', () => {
        
        it('should be able to find conversation by handle', async () => {
            let mockRes = {
                _pagination: {"prev":null,"next":"https://api2.frontapp.com/conversations?q[statuses][0]=unassigned&page_token=e0b5767cb0f1100743d46f67fcd765cade3cabee2ea868f16eea96c6b785276fa2e3311e7b940bf9461c8e669d75b2a618c1cb5819fddae23bfd002f024cb4a6",}, 
                _results: [{"_links":{"self":"https://api2.frontapp.com/conversations/cnv_1dhr9zv","related":{"events":"https://api2.frontapp.com/conversations/cnv_1dhr9zv/events","followers":"https://api2.frontapp.com/conversations/cnv_1dhr9zv/followers","messages":"https://api2.frontapp.com/conversations/cnv_1dhr9zv/messages","comments":"https://api2.frontapp.com/conversations/cnv_1dhr9zv/comments","inboxes":"https://api2.frontapp.com/conversations/cnv_1dhr9zv/inboxes"}},"id":"cnv_1dhr9zv","subject":"","status":"unassigned","assignee":null,"recipient":{"_links":{"related":{"contact":"https://api2.frontapp.com/contacts/crd_9lko6z"}},"handle":"2650394488365016","role":"from"},"tags":[],"last_message":{"_links":{"self":"https://api2.frontapp.com/messages/msg_2n3ww8z","related":{"conversation":"https://api2.frontapp.com/conversations/cnv_1dhr9zv"}},"id":"msg_2n3ww8z","type":"facebook","is_inbound":true,"created_at":1557493131.825,"blurb":"Doakan ibu sya supaya tetap sehat.","body":"<div>Doakan ibu sya supaya tetap sehat.</div>","text":"Doakan ibu sya supaya tetap sehat.","is_draft":false,"error_type":null,"metadata":{},"author":null,"recipients":[{"_links":{"related":{"contact":"https://api2.frontapp.com/contacts/crd_9lko6z"}},"handle":"2650394488365016","role":"from"},{"_links":{"related":{"contact":null}},"handle":"518306648595205","role":"to"}],"attachments":[]},"created_at":1557493135.529,"is_private":false}],
            }
            nock('https://api2.frontapp.com')
                .get(/\/inboxes\/.+?\/conversations/)
                .reply(200, mockRes)
            let c = await app.findConversationByHandle(
                mockRes._results[0].recipient.handle,
                mockRes._results[0].last_message.type
            )
            assert(c.id === mockRes._results[0].id)
        })
            
    })

    describe('tagConversation()', () => {
        
        it('should be able to update conversation tags', async () => {
            nock('https://api2.frontapp.com')
                .patch(/\/conversations.*/)
                .reply(204, undefined)
            let res = await app.tagConversation('CONV_ID', ['tag1', 'tag2'])
            assert(res === undefined)
        })
            
    })

})