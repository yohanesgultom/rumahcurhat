'use strict'
module.exports = (sequelize, DataTypes) => {
  const Session = sequelize.define('Session', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: DataTypes.STRING,
    channel: DataTypes.STRING,
    seq: DataTypes.INTEGER,
    age: DataTypes.STRING,
    location: DataTypes.STRING,
    conversationId: DataTypes.STRING,
    status: {
      type: DataTypes.TINYINT,
      defaultValue: 0,
    },
    followUpStatus: {
      type: DataTypes.TINYINT,
      defaultValue: 0,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {})

  Session.associate = function(models) {
    Session.hasMany(models.Message, {
      foreignKey: 'sessionId',
      as: 'messages',
    })
  }

  /** CONSTANTS */

  Session.StatusCode = {
    NEW: 0,
    HANDOVERED: 1,
  }

  Session.FollowUpStatusCode = {
    NEW: 0,
    IN_PROGRESS: 1,
    DONE: 2,
  }

  return Session
}