'use strict'

const assert = require('assert'),
    chats = require('../chat.json'),
    LineHelper = require('../line')


describe('Line', () => {
       
    describe('buildOptionQuickReplyMessages()', () => {
        
        it('should build correct messages', async () => {
            const helper = new LineHelper()
            const expectedMessages = [
                {
                    type: 'text',
                    text: 'Apakah kamu tau klo Tuhan sayang sama kamu?',
                    quickReply: {                        
                        items: [
                            {type: 'action', action: { type: 'message', label: 'Tidak', text: 'Tidak'}},
                            {type: 'action', action: { type: 'message', label: 'Ya', text: 'Ya'}},
                        ]
                    }
                },
            ]            
            let messages = helper.buildOptionQuickReplyMessages(chats[11])
            assert.deepEqual(messages, expectedMessages)
        })

        it('should build correct messages when one of label exceed 20 chars', async () => {
            const helper = new LineHelper()
            const expectedMessages = [
                {
                    type: 'text',
                    text: 'Apa Masalahmu?',
                },
                {
                    type: 'text',
                    text: '* Hubungan dengan keluarga\n* Masalah percintaan\n* Tujuan Hidup\n* Lainnya',
                    quickReply: {                        
                        items: [
                            {type: 'action', action: { type: 'message', label: 'Hubungan dengan ke..', text: 'Hubungan dengan keluarga'}},
                            {type: 'action', action: { type: 'message', label: 'Masalah percintaan', text: 'Masalah percintaan'}},
                            {type: 'action', action: { type: 'message', label: 'Tujuan Hidup', text: 'Tujuan Hidup'}},
                            {type: 'action', action: { type: 'message', label: 'Lainnya', text: 'Lainnya'}},
                        ]
                    }
                },
            ]            
            let messages = helper.buildOptionQuickReplyMessages(chats[4])
            assert.deepEqual(messages, expectedMessages)
        })
        
    })
    
})