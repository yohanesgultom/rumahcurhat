/**
 * Create and run multi-channel chatbot server
 * This server is NOT designed to be run on multiple parallel instances because
 * the duplicate request tracker stored data in memory
 */

'use strict'

const
    express = require('express'),
    bodyParser = require('body-parser'),    
    Line = require('@line/bot-sdk'),
    request = require('request'),
    ChatBot = require('./chatbot'),
    FrontApp = require('./frontapp'),
    FacebookHelper = require('./facebook'),
    LineHelper = require('./line'),
    models = require('./models/index')    

/**
* Build chatbot web server
*/
class Server {
    constructor(config, facebookHelper, lineHelper) {
        const bot = new ChatBot(
            JSON.parse(config.CHAT),
            JSON.parse(config.CHAT_FOLLOW_UP),
            config.COMMAND_RESTART_BOT
        )
        const app = express()
        this.handledRequestIds = {FB: [], LINE: []} // request ids tracker
        this.maxRequestIds = 200 // max number of request ids stored
        this.removeRequestIds = 50 // number of ids to remove when cache size == maxRequestIds
    
        // parse body to json when content is `application/json` type 
        // or request headers does not include `x-hub-signature`
        app.use(bodyParser.json({
            type: req => req.headers['content-type'] == 'aplication/json' 
                || (
                    !req.headers['x-hub-signature'] // fb messenger
                    && !req.headers['x-line-signature'] // line
                )
        }))
        
        // Use raw body parser when headers include `x-hub-signature`
        app.use(bodyParser.raw({
            type: req => !!req.headers['x-hub-signature'] || !!req.headers['x-line-signature']
        }))

        /* Middlewares */

        const verifySignature = (req, res, next) => {
            const signature = req.headers['x-hub-signature']
            const body = Buffer.isBuffer(req.body) ? req.body : ''
            if(signature !== facebookHelper.getSignature(body)) {
                res.status(400).send('Invalid signature')
            } else {
                next()
            }    
        }    

        // home: just to check if server is running
        app.get('/', (req, res) => {        
            res.send('Hello world')
        })

        // reload config from database
        app.post('/reloadConfig', (req, res) => {
            models.Config.findAll().then(configs => {
                // config
                configs.forEach(c => config[c.key] = c.value)
                
                // bot
                bot.messages = JSON.parse(config.CHAT)
                bot.followUpMessages = JSON.parse(config.CHAT_FOLLOW_UP)
                bot.restartCommand = config.COMMAND_RESTART_BOT || 'ulangi bot'

                // helpers            
                facebookHelper.apiURL = config.FB_MESSAGING_API_URL
                facebookHelper.appSecret = config.FB_APP_SECRET
                facebookHelper.pageToken = config.FB_PAGE_TOKEN
                facebookHelper.frontApp.apiUrl = config.FRONTAPP_API_URL
                facebookHelper.frontApp.apiToken = config.FRONTAPP_TOKEN
                facebookHelper.frontApp.inboxId = config.FRONTAPP_INBOX_ID

                let lineConfig = {
                    channelAccessToken: config.LINE_TOKEN,
                    channelSecret: config.LINE_SECRET,
                }
                // enable channel only if config available
                if (lineConfig.channelAccessToken && lineConfig.channelSecret) {
                    lineHelper.client = new Line.Client(lineConfig)
                    lineHelper.middleware = Line.middleware(lineConfig)
                }
                res.send('Config reloaded')
            })
            .catch(err => {
                console.error(err)
                res.status(500).send(err.message)
            })
        })

        /**********  FACEBOOK MESSENGER ROUTES  ****************/

        // verification
        app.get('/webhook', (req, res) => {
            const VERIFY_TOKEN = config.FB_VERIFY_TOKEN,
            mode = req.query['hub.mode'],
            token = req.query['hub.verify_token'],
            challenge = req.query['hub.challenge']
            if (mode && token) {
                if (mode === 'subscribe' && token === VERIFY_TOKEN) {
                    // console.debug('WEBHOOK_VERIFIED')
                    res.status(200).send(challenge)    
                } else {
                    res.sendStatus(403)      
                }
            }
        })
        
        // capture event
        app.post('/webhook', verifySignature, async (req, res) => {  
            // Checks this is an event from a page subscription
            let body = JSON.parse(req.body)
            if (body.object === 'page') {
                // response first so the server does not
                // think the request is having problem and retry
                // sending the request
                res.status(200).send('EVENT_RECEIVED')

                // handle duplicate requests
                let requestId = req.headers['x-hub-signature']
                if (this.handledRequestIds.FB.indexOf(requestId) >= 0) {
                    // no need to handle
                    return                 
                } else {
                    this.handledRequestIds.FB.push(requestId)
                    if (this.handledRequestIds.FB.length >= this.maxRequestIds) {
                        this.handledRequestIds.FB.splice(0, this.removeRequestIds)
                    }
                }

                // Iterates over each entry - there may be multiple if batched
                for (let i=0; i < body.entry.length; i++) {          
                    // Gets the message. entry.messaging is an array, but 
                    // will only ever contain one message, so we get index 0
                    let event = body.entry[i].messaging[0]
                    // console.debug(event)
                    
                    let input = null
                    if (event.postback) { 
                        // buttons
                        input = event.postback.payload
                    } else if (event.message) { 
                        if (event.message.quick_reply) {
                            // quick reply
                            input = event.message.quick_reply.payload
                        } else {
                            // plain message
                            input = event.message.text
                        }
                    }

                    try {
                        let chat = await bot.chat(event.sender.id, input, 'FB')
                        await facebookHelper.sendChat(event.sender.id, chat)
                    } catch (e) {
                        console.error(e)
                    }
                }
                
            } else {
                // Returns a '404 Not Found' 
                // if event is not from a page subscription
                res.sendStatus(404)
            }
            
        })
        
        /**********  LINE ROUTES  ****************/

        app.post('/callback', lineHelper.middleware, async (req, res) => {
            // response first so the server does not
            // think the request is having problem and retry
            // sending the request
            res.status(200).send('EVENT_RECEIVED')
            const events = req.body.events

            // handle duplicate requests
            let requestId = req.headers['x-line-signature']
            if (this.handledRequestIds.LINE.indexOf(requestId) >= 0) {
                // no need to handle
                return                 
            } else {
                this.handledRequestIds.LINE.push(requestId)
                if (this.handledRequestIds.LINE.length >= this.maxRequestIds) {
                    this.handledRequestIds.LINE.splice(0, this.removeRequestIds)
                }
            }

            // if LINE_EXISTING_WEBHOOK provided, forward request
            if (config.LINE_EXISTING_WEBHOOK) {
                request.post({
                    uri: config.LINE_EXISTING_WEBHOOK, 
                    headers: {
                        'x-line-signature': req.header('x-line-signature'),
                        'user-agent': req.header('user-agent'),
                    }, 
                    json: true, 
                    body: req.body,
                    rejectUnauthorized: false,
                }, (error, httpRes) => {
                    if (error) {
                        console.debug(httpRes)                        
                        console.error(error)                        
                    }                    
                })  
            }

            for (let i=0; i < events.length; i++) {
                try {
                    const event = events[i]                
                    // console.debug(event)
                    const input = event.message.text
                    let chat = await bot.chat(event.source.userId, input, 'LINE')                
                    // console.debug(chat)                                        
                    await lineHelper.sendChat(event.source.userId, chat)
                } catch (e) {
                    console.error(e)
                }
            }
        })

        // init class variables
        this.bot = bot
        this.app = app
        this.facebookHelper = facebookHelper
        this.lineHelper = lineHelper
    }
}


/** Main */

if (require.main === module) {
    // read config from database
    models.Config.findAll().then(configs => {
        // config
        const config = {}
        configs.forEach(c => config[c.key] = c.value)
        config.PORT = config.PORT || 1337
        config.FIRST_MESSAGE_DELAY = config.FIRST_MESSAGE_DELAY || 5000
        config.NEXT_MESSAGE_DELAY = config.NEXT_MESSAGE_DELAY || 3000    
        
        // helpers
        let facebookHelper = {}
        if (config.FB_MESSAGING_API_URL && config.FB_APP_SECRET && config.FB_PAGE_TOKEN) {
            let frontAppFBHelper = {}
            if (config.FRONTAPP_API_URL && config.FRONTAPP_TOKEN && config.FRONTAPP_FB_INBOX_ID) {
                frontAppFBHelper = new FrontApp(
                    config.FRONTAPP_API_URL, 
                    config.FRONTAPP_TOKEN, 
                    config.FRONTAPP_FB_INBOX_ID
                )
            }
    
            facebookHelper = new FacebookHelper(
                config.FB_MESSAGING_API_URL, 
                config.FB_APP_SECRET, 
                config.FB_PAGE_TOKEN, 
                frontAppFBHelper,
                config.FIRST_MESSAGE_DELAY,
                config.NEXT_MESSAGE_DELAY
            )
        }

        let lineHelper
        if (config.LINE_TOKEN && config.LINE_SECRET) {
            let lineConfig = {
                channelAccessToken: config.LINE_TOKEN,
                channelSecret: config.LINE_SECRET,
            }
            let frontAppLINEHelper = {}
            if (config.FRONTAPP_API_URL && config.FRONTAPP_TOKEN && config.FRONTAPP_LINE_INBOX_ID) {
                frontAppLINEHelper = new FrontApp(
                    config.FRONTAPP_API_URL, 
                    config.FRONTAPP_TOKEN, 
                    config.FRONTAPP_LINE_INBOX_ID
                )
            }            
            lineHelper = new LineHelper(
                new Line.Client(lineConfig),
                Line.middleware(lineConfig),
                frontAppLINEHelper,
                config.FIRST_MESSAGE_DELAY,
                config.NEXT_MESSAGE_DELAY
            )
        } else {
            lineHelper = new LineHelper(null, function () {})
        }

        // build and run server
        let server = new Server(config, facebookHelper, lineHelper)
        server.app.listen(config.PORT, () => {
            console.log(`Webhook is listening on port ${config.PORT}..`)
        })
    })
} else {
    module.exports = Server
}

