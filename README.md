# Rumah Curhat

RumahCurhat multi-channel chat bot server. Supported channels: 

1. [Facebook Messenger](https://developers.facebook.com/docs/messenger-platform)
2. [LINE Messenger](https://developers.line.biz/en/docs/messaging-api/)

## Prerequisites

Make sure these softwares are available in machine whare you want to run this chatbot server:

1. Node.js >= 8.11.1
1. MySQL >= 14.14 Distrib 5.7.26
1. SQLite3 (Optional)

## Installation

Follow this steps for **fresh** installation:

1. Copy `config.example.json` to `config.json` and update its value accordingly
1. Install node dependencies `npm i`
1. Run unit test `npm test`
1. On Linux or Mac, init database with `npm run db:init` which creates table and populates `configs` table with value from `config.json`. Also make sure `chat.json` and `chat-follow-up.json` are available as the source of chat messages
1. If you use other OS, create database manually, using `schema_with_data_sample.sql`
1. If everything looks good, run server `npm start`

## Customizing CHAT

To customize chat behavior, you can edit JSON value stored in `CHAT` within `Configs` table. Some guidelines that need to be followed:

1. You can edit the string value in `messages` as you wish. But never changes the structure (it has to be a list)
2. Do not edit `variable` value unless the value is a column in `Sessions` table
3. Do not remove any field
4. Do not change data structure
5. For chat with `type` = `OPTIONS`, make sure each item in `options` fields point to existing chat id. Eg. `"options": { "option A": 11, "option B": 22 }` where `11` and `22` are a valid chat

> `chat*.json` files SHOULD NOT be edited because it is used in automated testing

To update the JSON `CHAT` in database make sure you use `utf8mb4` (to enable emoji) and disable backlash escape (to avoid `\"` converted to `\\"`):

```
SET character_set_client = 'utf8mb4';
SET sql_mode = NO_BACKSLASH_ESCAPES;
```

Depends on how you input the emoji, it may converted to different format in database. The safest way to store the emoji is by using its [surrogate pair](https://thekevinscott.com/emojis-in-javascript/) format. These are some example of emojis and their surrogate pairs:

```
😊 = \uD83D\uDE0A
😁 = \uD83D\uDE01
❤ = \u2764
✊ = \u270A
```
> To convert from emoji to surrogate pair or vice versa, use https://lab.gultom.me/emoji.html


## API

`POST /reloadConfig`

Clear config cache and reload from database

`GET /webhook`

Facebook verification endpoint. Uses FB_VERIFY_TOKEN config value as verification token

`POST /webhook`

Facebook (secured) webhook endpoint to capture event

`POST /callback`

LINE (secured) callback endpoint to capture event

## Cron Jobs

Cron jobs are node scripts that need to be run using cron-like scheduler:

1. `/follow-up.js` Send follow up messages to user